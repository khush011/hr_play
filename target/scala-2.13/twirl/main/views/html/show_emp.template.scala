
package views.html

import _root_.play.twirl.api.TwirlFeatureImports._
import _root_.play.twirl.api.TwirlHelperImports._
import _root_.play.twirl.api.Html
import _root_.play.twirl.api.JavaScript
import _root_.play.twirl.api.Txt
import _root_.play.twirl.api.Xml
import models._
import controllers._
import play.api.i18n._
import views.html._
import play.api.templates.PlayMagic._
import play.api.mvc._
import play.api.data._

object show_emp extends _root_.play.twirl.api.BaseScalaTemplate[play.twirl.api.HtmlFormat.Appendable,_root_.play.twirl.api.Format[play.twirl.api.HtmlFormat.Appendable]](play.twirl.api.HtmlFormat) with _root_.play.twirl.api.Template7[collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],Flash,play.twirl.api.HtmlFormat.Appendable] {

  /**/
  def apply/*1.2*/(fname:collection.mutable.ListBuffer[String],lname:collection.mutable.ListBuffer[String],
age:collection.mutable.ListBuffer[String],
salary:collection.mutable.ListBuffer[String],
dept:collection.mutable.ListBuffer[String],
location:collection.mutable.ListBuffer[String])(implicit flash:Flash):play.twirl.api.HtmlFormat.Appendable = {
    _display_ {
      {


Seq[Any](format.raw/*6.1*/("""

"""),_display_(/*8.2*/main("Employee List")/*8.23*/{_display_(Seq[Any](format.raw/*8.24*/("""
"""),format.raw/*9.1*/("""<div style="padding: 1cm; background-color:mistyrose;height:30cm">
    <form style="padding:0.5cm; float:right" action=""""),_display_(/*10.55*/routes/*10.61*/.HR.login),format.raw/*10.70*/("""">
        <button class="btn btn-primary" type="submit">Log Out!</button>
    </form>
    <form style="padding:0.5cm; float:right" action=""""),_display_(/*13.55*/routes/*13.61*/.HR.add_employee),format.raw/*13.77*/("""">
        <button class="btn btn-primary" type="submit">Add Employee</button>
    </form>
    <span style="color:red">"""),_display_(/*16.30*/flash/*16.35*/.get("msg")),format.raw/*16.46*/("""</span>
    <span style="color:green">"""),_display_(/*17.32*/flash/*17.37*/.get("msg1")),format.raw/*17.49*/("""</span>
<table class="table" style="border:solid 1px;border-color: white; background-color:lavender">
    <thead class="thead-dark">
    <tr>
        <th scope="col">Id</th>
        <th scope="col">First Name</th>
        <th scope="col">Last Name</th>
        <th scope="col">Age</th>
        <th scope="col">Salary</th>
        <th scope="col">Department</th>
        <th scope="col">Location</th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>
    """),_display_(/*32.6*/for(i<- 0 to (fname.length)-1) yield /*32.36*/{_display_(Seq[Any](format.raw/*32.37*/("""
    """),format.raw/*33.5*/("""<tr>
        <th style="border:solid 1px, border-color: white" scope="row">"""),_display_(/*34.72*/(i+1)),format.raw/*34.77*/("""</th>
        <td style="border:solid 1px;border-color: white">"""),_display_(/*35.59*/fname(i)),format.raw/*35.67*/("""</td>
        <td style="border:solid 1px;border-color: white">"""),_display_(/*36.59*/lname(i)),format.raw/*36.67*/("""</td>
        <td style="border:solid 1px;border-color: white">"""),_display_(/*37.59*/age(i)),format.raw/*37.65*/("""</td>
        <td style="border:solid 1px;border-color: white">"""),_display_(/*38.59*/salary(i)),format.raw/*38.68*/("""</td>
        <td style="border:solid 1px;border-color: white">"""),_display_(/*39.59*/dept(i)),format.raw/*39.66*/("""</td>
        <td style="border:solid 1px;border-color: white">"""),_display_(/*40.59*/location(i)),format.raw/*40.70*/("""</td>
        <td style="border:solid 1px;border-color: white">
            <div style="border-">
            <form method="post" action=""""),_display_(/*43.42*/routes/*43.48*/.HR.update_emp),format.raw/*43.62*/("""">
                <button style="float:left" class="btn btn-primary" name="index" type="submit" value=""""),_display_(/*44.103*/i),format.raw/*44.104*/("""">Update</button>
            </form>
            <form method="post" action=""""),_display_(/*46.42*/routes/*46.48*/.HR.delete),format.raw/*46.58*/("""">
                <button style="margin-left:10px" class="btn btn-primary" name="index" type="submit" value=""""),_display_(/*47.109*/i),format.raw/*47.110*/("""">Delete</button>
            </form>
            </div>
        </td>
    </tr>
    """)))}),format.raw/*52.6*/("""
    """),format.raw/*53.5*/("""</tbody>
</table>
</div>

""")))}))
      }
    }
  }

  def render(fname:collection.mutable.ListBuffer[String],lname:collection.mutable.ListBuffer[String],age:collection.mutable.ListBuffer[String],salary:collection.mutable.ListBuffer[String],dept:collection.mutable.ListBuffer[String],location:collection.mutable.ListBuffer[String],flash:Flash): play.twirl.api.HtmlFormat.Appendable = apply(fname,lname,age,salary,dept,location)(flash)

  def f:((collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String],collection.mutable.ListBuffer[String]) => (Flash) => play.twirl.api.HtmlFormat.Appendable) = (fname,lname,age,salary,dept,location) => (flash) => apply(fname,lname,age,salary,dept,location)(flash)

  def ref: this.type = this

}


              /*
                  -- GENERATED --
                  SOURCE: app/views/show_emp.scala.html
                  HASH: 5b4e4037cd0c8f4b0c1ee48c3b181b78273f4616
                  MATRIX: 959->1|1345->294|1373->297|1402->318|1440->319|1467->320|1615->441|1630->447|1660->456|1828->597|1843->603|1880->619|2027->739|2041->744|2073->755|2139->794|2153->799|2186->811|2682->1281|2728->1311|2767->1312|2799->1317|2902->1393|2928->1398|3019->1462|3048->1470|3139->1534|3168->1542|3259->1606|3286->1612|3377->1676|3407->1685|3498->1749|3526->1756|3617->1820|3649->1831|3815->1970|3830->1976|3865->1990|3998->2095|4021->2096|4127->2175|4142->2181|4173->2191|4312->2302|4335->2303|4451->2389|4483->2394
                  LINES: 21->1|30->6|32->8|32->8|32->8|33->9|34->10|34->10|34->10|37->13|37->13|37->13|40->16|40->16|40->16|41->17|41->17|41->17|56->32|56->32|56->32|57->33|58->34|58->34|59->35|59->35|60->36|60->36|61->37|61->37|62->38|62->38|63->39|63->39|64->40|64->40|67->43|67->43|67->43|68->44|68->44|70->46|70->46|70->46|71->47|71->47|76->52|77->53
                  -- GENERATED --
              */
          